window.addEventListener("load", () => {
  const rotatingDiv = document.querySelector(".rotating_div");
  const leftImage = document.querySelector(".left_image");

  function rotateAroundLeftImage() {
    const leftImageRect = leftImage.getBoundingClientRect();
    const leftImageCenterX = leftImageRect.left + leftImageRect.width / 5;
    const leftImageCenterY = leftImageRect.top + leftImageRect.height / 5;

    const angle = (Date.now() / 100) % 360; // Change the divisor to adjust rotation speed

    const radius = 230; // Change the radius to control the distance from the center of .left_image
    const x = leftImageCenterX + radius * Math.cos(angle * (Math.PI / 30));
    const y = leftImageCenterY + radius * Math.sin(angle * (Math.PI / 30));

    rotatingDiv.style.left = x - rotatingDiv.offsetWidth / 1 + "px"; // Double of y (I changed it)
    rotatingDiv.style.top = y - rotatingDiv.offsetHeight / 0.5 + "px"; // Half of x (I changed it)

    requestAnimationFrame(rotateAroundLeftImage);
  }

  rotateAroundLeftImage();
});

// -----Hire Me Effect------
let isEffectActive = false;

function shadowInterval() {
  let targetEffect = document.querySelector("#hire_me");
  targetEffect.classList.toggle("hire_effect", isEffectActive);
  isEffectActive = !isEffectActive;
}

// Execute shadowInterval function on page load
document.addEventListener("DOMContentLoaded", function () {
  shadowInterval();

  // Start the interval after the initial execution
  setInterval(shadowInterval, 5000); // Execute the shadowInterval function every 2 seconds (2000 milliseconds)
});

function hoverSun() {
  let button1 = document.querySelector("#button_1");
  button1.style.transform = "translateX(0)";
  let button2 = document.querySelector("#button_2");
  button2.style.transform = "translateX(0)";
  let button3 = document.querySelector("#button_3");
  button3.style.transform = "translateX(0)";
  let button4 = document.querySelector("#button_4");
  button4.style.transform = "translateX(0)";
  // ------------------------

  const spans = document.querySelectorAll(".span_yellow");
  const colors = ["#007ced", "orange", "yellow", "violet"]; // Add more colors as needed

  spans.forEach((span, index) => {
    span.style.color = colors[index % colors.length];
  });
}
function hoverSunOut() {
  let button1 = document.querySelector("#button_1");
  button1.style.transform = "translateX(-600px)";
  let button2 = document.querySelector("#button_2");
  button2.style.transform = "translateX(600px)";
  let button3 = document.querySelector("#button_3");
  button3.style.transform = "translateX(-300px)";
  let button4 = document.querySelector("#button_4");
  button4.style.transform = "translateX(300px)";
  // ------------------
  const spans = document.querySelectorAll(".span_yellow");
  const colors = ["white", "white", "white", "white"]; // Add more colors as needed

  spans.forEach((span, index) => {
    span.style.color = colors[index % colors.length];
  });
}

// -----------------------------------
//            Black Hole
// -----------------------------------

blackhole("#blackhole");

function blackhole(element) {
  var h = $(element).height(),
    w = $(element).width(),
    cw = w,
    ch = h,
    maxorbit = 255, // distance from center
    centery = ch / 2,
    centerx = cw / 2;

  var startTime = new Date().getTime();
  var currentTime = 0;

  var stars = [],
    collapse = false, // if hovered
    expanse = false; // if clicked

  var canvas = $("<canvas/>").attr({ width: cw, height: ch }).appendTo(element),
    context = canvas.get(0).getContext("2d");

  context.globalCompositeOperation = "multiply";

  function setDPI(canvas, dpi) {
    // Set up CSS size if it's not set up already
    if (!canvas.get(0).style.width)
      canvas.get(0).style.width = canvas.get(0).width + "px";
    if (!canvas.get(0).style.height)
      canvas.get(0).style.height = canvas.get(0).height + "px";

    var scaleFactor = dpi / 96;
    canvas.get(0).width = Math.ceil(canvas.get(0).width * scaleFactor);
    canvas.get(0).height = Math.ceil(canvas.get(0).height * scaleFactor);
    var ctx = canvas.get(0).getContext("2d");
    ctx.scale(scaleFactor, scaleFactor);
  }

  function rotate(cx, cy, x, y, angle) {
    var radians = angle,
      cos = Math.cos(radians),
      sin = Math.sin(radians),
      nx = cos * (x - cx) + sin * (y - cy) + cx,
      ny = cos * (y - cy) - sin * (x - cx) + cy;
    return [nx, ny];
  }

  setDPI(canvas, 192);

  var star = function () {
    // Get a weighted random number, so that the majority of stars will form in the center of the orbit
    var rands = [];
    rands.push(Math.random() * (maxorbit / 2) + 1);
    rands.push(Math.random() * (maxorbit / 2) + maxorbit);

    this.orbital =
      rands.reduce(function (p, c) {
        return p + c;
      }, 0) / rands.length;
    // Done getting that random number, it's stored in this.orbital

    this.x = centerx; // All of these stars are at the center x position at all times
    this.y = centery + this.orbital; // Set Y position starting at the center y + the position in the orbit

    this.yOrigin = centery + this.orbital; // this is used to track the particles origin

    this.speed = ((Math.floor(Math.random() * 2.5) + 1.5) * Math.PI) / 180; // The rate at which this star will orbit
    this.rotation = 0; // current Rotation
    this.startRotation =
      ((Math.floor(Math.random() * 360) + 1) * Math.PI) / 180; // Starting rotation.  If not random, all stars will be generated in a single line.

    this.id = stars.length; // This will be used when expansion takes place.

    this.collapseBonus = this.orbital - maxorbit * 0.7; // This "bonus" is used to randomly place some stars outside of the blackhole on hover
    if (this.collapseBonus < 0) {
      // if the collapse "bonus" is negative
      this.collapseBonus = 0; // set it to 0, this way no stars will go inside the blackhole
    }

    stars.push(this);
    this.color = "rgba(255,255,255," + (1 - this.orbital / 255) + ")"; // Color the star white, but make it more transparent the further out it is generated

    this.hoverPos = centery + maxorbit / 2 + this.collapseBonus; // Where the star will go on hover of the blackhole
    this.expansePos =
      centery + (this.id % 100) * -10 + (Math.floor(Math.random() * 20) + 1); // Where the star will go when expansion takes place

    this.prevR = this.startRotation;
    this.prevX = this.x;
    this.prevY = this.y;

    // The reason why I have yOrigin, hoverPos and expansePos is so that I don't have to do math on each animation frame.  Trying to reduce lag.
  };
  star.prototype.draw = function () {
    // the stars are not actually moving on the X axis in my code.  I'm simply rotating the canvas context for each star individually so that they all get rotated with the use of less complex math in each frame.

    if (!expanse) {
      this.rotation = this.startRotation + currentTime * this.speed;
      if (!collapse) {
        // not hovered
        if (this.y > this.yOrigin) {
          this.y -= 2.5;
        }
        if (this.y < this.yOrigin - 4) {
          this.y += (this.yOrigin - this.y) / 10;
        }
      } else {
        // on hover
        this.trail = 1;
        if (this.y > this.hoverPos) {
          this.y -= (this.hoverPos - this.y) / -5;
        }
        if (this.y < this.hoverPos - 4) {
          this.y += 2.5;
        }
      }
    } else {
      this.rotation = this.startRotation + currentTime * (this.speed / 2);
      if (this.y > this.expansePos) {
        this.y -= Math.floor(this.expansePos - this.y) / -140;
      }
    }

    context.save();
    context.fillStyle = this.color;
    context.strokeStyle = this.color;
    context.beginPath();
    var oldPos = rotate(centerx, centery, this.prevX, this.prevY, -this.prevR);
    context.moveTo(oldPos[0], oldPos[1]);
    context.translate(centerx, centery);
    context.rotate(this.rotation);
    context.translate(-centerx, -centery);
    context.lineTo(this.x, this.y);
    context.stroke();
    context.restore();

    this.prevR = this.rotation;
    this.prevX = this.x;
    this.prevY = this.y;
  };

  $(".centerHover").on("click", function () {
    collapse = false;
    expanse = true;

    $(this).addClass("open");
    $(".fullpage").addClass("open");
    setTimeout(function () {
      $(".header .welcome").removeClass("gone");
    }, 500);
  });
  $(".centerHover").on("mouseover", function () {
    if (expanse == false) {
      collapse = true;
    }
  });
  $(".centerHover").on("mouseout", function () {
    if (expanse == false) {
      collapse = false;
    }
  });

  window.requestFrame = (function () {
    return (
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      function (callback) {
        window.setTimeout(callback, 1000 / 60);
      }
    );
  })();

  function loop() {
    var now = new Date().getTime();
    currentTime = (now - startTime) / 50;

    context.fillStyle = "rgba(25,25,25,0.2)"; // somewhat clear the context, this way there will be trails behind the stars
    context.fillRect(0, 0, cw, ch);

    for (var i = 0; i < stars.length; i++) {
      // For each star
      if (stars[i] != stars) {
        stars[i].draw(); // Draw it
      }
    }

    requestFrame(loop);
  }

  function init(time) {
    context.fillStyle = "rgba(25,25,25,1)"; // Initial clear of the canvas, to avoid an issue where it all gets too dark
    context.fillRect(0, 0, cw, ch);
    for (var i = 0; i < 2500; i++) {
      // create 2500 stars
      new star();
    }
    loop();
  }
  init();
}
// -----------------------------------------------------
//                 Blackhole Effect Hidden
// -----------------------------------------------------
function growBlackhole() {
  // document.getElementById("wrapper_new").style = "display: none";
  // document.getElementById("wrapper").style = "display: none";
  document.getElementById("skills").style = "display: none";
  let debrisElements = document.querySelectorAll(".debris");

  debrisElements.forEach(function (element) {
    element.style.display = "none";
  });

  let wrapper0 = document.getElementById("wrapper_0");
  wrapper0.style =
    "display: block; opacity: 0; transition: opacity 0.5s ease; cursor: url('./images/astronaut_cursor.png'), auto;";
  document.getElementById("big_blackhole").style =
    "animation: growBlackhole 600s infinite linear; cursor: none;";

  // Adding a slight delay to allow the display change to take effect before updating opacity
  setTimeout(function () {
    wrapper0.style.opacity = "1";
  }, 100);
}

function cursorGoneBlack() {
  document.getElementById("wrapper_0").style.cursor = "none";
}
